﻿using BaristaLabs.ChromeDevTools.Runtime;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ThreeCS
{
    class Program
    {
        static void Main(string[] args)
        {
            MainAsync().Wait();

            Console.WriteLine("Done :)");
            Console.ReadKey();
        }

        static async Task MainAsync()
        {
            var url = "http://postman-echo.com/headers";

            var chromeInstance = Chrome.OpenChrome(headless: true);

            var session = (await chromeInstance.GetActiveSessions()).ToList();
            var sessionInfo = session.Last();

            var x = await NavigateToPageInNewSession(sessionInfo, url);

            await chromeInstance.CloseSession(sessionInfo);

            chromeInstance.Dispose();

            SavePdf(x, url);
        }

        private static void SavePdf(string base64BinaryStr, string url)
        {
            byte[] bytes = Convert.FromBase64String(base64BinaryStr);
            var stream = new MemoryStream(bytes);

            var pathSafeUrl = new Uri(url).Host;
            var savePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop), $"{pathSafeUrl}_{Guid.NewGuid()}.pdf");
            using (var fileStream = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            {
                stream.CopyTo(fileStream);
            }
        }

        public static async Task<string> NavigateToPageInNewSession(ChromeSessionInfo sessionInfo, string address)
        {
            var s = new SemaphoreSlim(0, 1);
            string body = null;
            using (var session = new ChromeSession(sessionInfo.WebSocketDebuggerUrl))
            {

                await session.Page.Enable();
                await session.Network.Enable(new BaristaLabs.ChromeDevTools.Runtime.Network.EnableCommand());

                await session.Network.SetRequestInterceptionEnabled(new BaristaLabs.ChromeDevTools.Runtime.Network.SetRequestInterceptionEnabledCommand { Enabled = true });

                session.Network.SubscribeToRequestInterceptedEvent(async (e) =>
                {
                    e.Request.Headers.Add("test", "12345");
                    await session.Network.ContinueInterceptedRequest(new BaristaLabs.ChromeDevTools.Runtime.Network.ContinueInterceptedRequestCommand
                    {
                        InterceptionId = e.InterceptionId,
                        Headers = e.Request.Headers
                    });
                });

                session.Page.SubscribeToLoadEventFiredEvent(async (e) =>
                {
                    await Task.Delay(1000);

                    var pdfResponse = await session.Page.PrintToPDF(new BaristaLabs.ChromeDevTools.Runtime.Page.PrintToPDFCommand()
                    {
                        Landscape = true,
                        PrintBackground = true
                    });

                    body = pdfResponse.Data;
                    s.Release();
                });

                var navigateResult = await session.Page.Navigate(new BaristaLabs.ChromeDevTools.Runtime.Page.NavigateCommand
                {
                    Url = address
                });

                await s.WaitAsync();
                return body;
            }
        }
    }
}
