namespace BaristaLabs.ChromeDevTools.Runtime.Tracing
{
    using Newtonsoft.Json;

    /// <summary>
    /// Configuration for memory dump. Used only when &quot;memory-infra&quot; category is enabled.
    /// </summary>
    public sealed class MemoryDumpConfig
    {
    }
}